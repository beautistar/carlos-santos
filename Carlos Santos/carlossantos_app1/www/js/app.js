// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (cordova.platformId === 'ios' && window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
                window.open = cordova.InAppBrowser.open;
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

    })

    .config(function ($stateProvider, $urlRouterProvider, $cordovaInAppBrowserProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                cache: false,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.search', {
                url: '/search',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/search.html',
                        controller: 'MyCtrl'
                    }
                }
            })
            .state('app.wherepage', {
                url: '/wherepage',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/wherepage.html',
                        controller: 'MyCtrl'
                    }
                }

            })
            .state('app.aboutus', {
                url: '/aboutus',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/aboutus.html',
                        controller: 'MyCtrl'
                    }
                }
            })

            .state('app.browse', {
                url: '/browse',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/browse.html',
                        controller: 'MyCtrl'
                    }
                }
            })
            .state('app.playlists', {
                url: '/playlists',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'templates/playlists.html',
                        controller: 'MyCtrl'
                    }
                }
            })

            .state('app.single', {
                url: '/playlists/:playlistId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/playlist.html',
                        controller: 'MyCtrl'
                    }
                }
            })
            ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/playlists');
    });
