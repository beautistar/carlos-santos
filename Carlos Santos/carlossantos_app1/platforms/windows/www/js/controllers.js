﻿angular.module('starter.controllers', [])
    .run(function ($ionicPlatform, $ionicPopup, $interval, $cordovaFileTransfer, $http) {
        $ionicPlatform.ready(function () {
            setTimeout(function () {

                //    $http.get("http://cs.innfocus.pt/api/getVideo")
                //        .success(function (data) {
                //            debugger;
                //            var videoSrcApply = "http://cs.innfocus.pt" + data[0].video;
                //            var saveURL = localStorage["videoSrcApply"];
                //            var videoSrcLocalURL = localStorage["videoSrcLocalURL"];
                //            if (saveURL == videoUrl) {
                //                videoUrl = videoSrcLocalURL;
                //                localStorage.setItem('videoSrcApply', videoSrcApply);
                //            }
                //            else {
                //                localStorage.setItem('videoSrcApply', videoSrcApply);
                //                var url = videoSrcApply;
                //                var filename = url.split('/').pop().split('#')[0].split('?')[0];

                //                if (filename == "") {
                //                    filename = "onepub.mp4";
                //                }
                //                var targetPath = cordova.file.dataDirectory + filename;

                //                if ($ionicPlatform.is('android')) {
                //                    targetPath = cordova.file.externalApplicationStorageDirectory + filename;
                //                }
                //                var trustHosts = true;
                //                var options = {};

                //                $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                //                    .then(function (result) {
                //                        localStorage.setItem('videoSrcLocalURL', targetPath);
                //                        videoUrl = targetPath;
                //                    }, function (error) {
                //                        document.getElementById("custom-overlay").style.display = "none";
                //                        alert("An error has occurred: Code = " + error.code);
                //                        alert("upload error source " + error.source);
                //                        alert("upload error target " + error.target);
                //                    }, function (progress) {

                //                    });
                //            }
                //        })
                //        .error(function (data) {
                //        });


                navigator.splashscreen.hide();
            }, 300);

            document.addEventListener("offline", onOffline, false);

            function onOffline() {
                $ionicPopup.confirm({
                    title: "Internet Disconnected",
                    content: "The internet is disconnected on your device."
                })
                    .then(function (result) {
                        ionic.Platform.exitApp();
                    });
            }
        });
    })
    .controller('AppCtrl', function ($scope, $ionicModal, $timeout, $ionicHistory, $cordovaFile) {
        $scope.$on('$ionicView.afterEnter', function () {
            //setTimeout(function () {
            //    document.getElementById("custom-overlay").style.display = "none";
            //}, 3000);
            $ionicHistory.clearCache();
        });

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $ionicModal.fromTemplateUrl('templates/infoModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.infoModal = modal;
        });

        $ionicModal.fromTemplateUrl('templates/settingModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.settingModal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        $scope.closesettingModal = function () {
            $scope.settingModal.hide();
        };
        $scope.opensettingModal = function () {
            $scope.settingModal.show();
        };
        $scope.closeinfoModal = function () {
            $scope.infoModal.hide();
        };
        $scope.openinfoModal = function () {
            $scope.infoModal.show();
        };

        $scope.opensamplePDF = function () { 
            var url = "http://etd.lib.byu.edu/PDFCreation/AddingInternalLinksandMultimediaElements.pdf";
            var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');
            ref.addEventListener('loadstart', function (event) {
            });
        };
        $scope.buyOnline = function () {
            window.open('http://santosshoes.com/loja/?lang=pt-pt', '_system', 'location=yes'); return false;
        };

        $scope.openFacebook = function () {
            window.open('https://www.facebook.com/CarlosSantosShoes', '_system', 'location=yes'); return false;
            //var ref = cordova.InAppBrowser.open("https://www.facebook.com/CarlosSantosShoes/", '_blank', 'location=yes');
            //ref.addEventListener('loadstart', function (event) {
            //});
        };

        $scope.openPintrest = function () {
            window.open('https://www.pinterest.com/santosshoes/', '_system', 'location=yes'); return false;
        }; 

        $scope.openYoutube = function () {
            window.open('https://www.youtube.com/watch?v=cBxpX-q6Hf8', '_system', 'location=yes'); return false;
        };

        $scope.openInstagram = function () {
            window.open('https://www.instagram.com/carlossantosshoes/', '_system', 'location=yes'); return false;
        };


        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function () {
                $scope.closeLogin();
            }, 1000);
        };
    })

    .controller('PlaylistsCtrl', function ($scope, $http, $state, $ionicSideMenuDelegate, $ionicSlideBoxDelegate, $ionicHistory, $sce) {
        //$ionicSideMenuDelegate.canDragContent(false)
        $scope.clientHeight = "400px";
        $scope.HideVideo = false;
        $scope.videoSrc = "";
        $scope.IsProcess = false;
        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }
        $scope.loadMainText = function () {
            $http.get("http://cs.innfocus.pt/api/getVideo")
                .success(function (data) {
                    $scope.videoSrcApply = $scope.trustSrc("http://cs.innfocus.pt" + data[0].video);
                })
                .error(function (data) {
                });
            $http.get("http://cs.innfocus.pt/api/secondScreen")
                .success(function (data) {
                    $scope.MainscreenText = data;
                    setTimeout(function () {
                        $ionicSlideBoxDelegate.update();
                    }, 1000);
                    var element = document.getElementsByTagName("ion-view");
                    $scope.clientHeight = element[0].clientHeight + "px";
                })
                .error(function (data) {
                });
            if (!$scope.IsProcess) {
                setTimeout(function () {
                    $scope.HideVideo = true;
                    $scope.IsProcess = true;
                    $scope.$apply();
                }, 15000);

            }
        };
        $scope.next = function () {
            $ionicSlideBoxDelegate.next();
        };
        $scope.previous = function () {
            $ionicSlideBoxDelegate.previous();
        };

        // Called each time the slide changes
        $scope.slideChanged = function (index) {
            $scope.slideIndex = index;
        };
        //$scope.sliderOptions = {
        //    effect: 'slide',
        //    pagination: false,
        //    initialSlide: 0
        //}
        $scope.myActiveSlide = 1;

        $scope.openProductList = function () {
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
                historyRoot: true
            });
            $state.go('app.search', { cache: false, reload: true });
        };
        $scope.options = {

        };

        $scope.data = {};
    })

    .controller('PlaylistCtrl', function ($scope, $stateParams, $state) {
    })


    .controller('MyCtrl', function ($scope, $sce, $ionicSlideBoxDelegate, $ionicPlatform, $ionicHistory, $interval, $cordovaInAppBrowser, $state, $http, $cordovaFileTransfer, $timeout, $ionicModal, $cordovaFile, $ionicLoading) {

        $scope.clientHeight = "400px";
        $scope.HideVideo = false;
        $scope.videoSrc = "";
        $scope.IsProcess = false;
        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }
        $scope.loadMainText = function () {
            if (videoUrl === "" || videoUrl === undefined || videoUrl === null) {
                setTimeout(function () {
                    $http.get("http://cs.innfocus.pt/api/getVideo")
                        .success(function (data) {
                            var videoSrcApply = "http://cs.innfocus.pt" + data[0].video;
                            var saveURL = localStorage["videoSrcApply"];
                            var videoSrcLocalURL = localStorage["videoSrcLocalURL"];
                            //if (saveURL == $scope.videoSrcApply) {
                            //    document.getElementById("custom-overlay").style.display = "none";
                            //    $scope.videoSrcApply = videoSrcLocalURL;
                            //    localStorage.setItem('videoSrcApply', videoSrcApply);
                            //}
                            //else {
                            localStorage.setItem('videoSrcApply', videoSrcApply);
                            var url = videoSrcApply;
                            var filename = url.split('/').pop().split('#')[0].split('?')[0];

                            if (filename == "") {
                                filename = "onepub.mp4";
                            }
                            var targetPath = cordova.file.dataDirectory + filename;

                            if ($ionicPlatform.is('android')) {
                                targetPath = cordova.file.externalApplicationStorageDirectory + filename;
                            }

                            if ($ionicPlatform.is('ios')) {
                                //alert("ios1")
                                //debugger
                                //window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                                //    fs.root.getDirectory(
                                //        "carlossantos_app1",
                                //        {
                                //            create: true
                                //        },
                                //        function (dirEntry) {
                                //            debugger;
                                //            dirEntry.getFile(
                                //                "filename",
                                //                {
                                //                    create: true,
                                //                    exclusive: false
                                //                },
                                //                function gotFileEntry(fe) {
                                //                    var p = fe.toURL();
                                //                    fe.remove();
                                //                    ft = new FileTransfer();
                                //                    ft.download(
                                //                        encodeURI("http://ionicframework.com/img/ionic-logo-blog.png"),
                                //                        p,
                                //                        function (entry) {
                                //                            $ionicLoading.hide();
                                //                            $scope.imgFile = entry.toURL();
                                //                        },
                                //                        function (error) {
                                //                            $ionicLoading.hide();
                                //                            alert("Download Error Source -> " + error.source);
                                //                        },
                                //                        false,
                                //                        null
                                //                    );
                                //                },
                                //                function () {
                                //                    $ionicLoading.hide();
                                //                    alert("Get file failed");
                                //                }
                                //            );
                                //        }
                                //    );
                                //},
                                //    function () {
                                //        $ionicLoading.hide();
                                //        alert("Request for filesystem failed");
                                //    });
                                //alert("ios2")
                                //debugger;
                                //window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                                //    alert("call!")
                                //    fs.root.getDirectory(
                                //        "carlossantos_app1",
                                //        {
                                //            create: false
                                //        },
                                //        function (dirEntry) {
                                //            debugger;
                                //            alert(dirEntry);
                                //            dirEntry.getFile(
                                //                "filename",
                                //                {
                                //                    create: false,
                                //                    exclusive: false
                                //                },
                                //                function gotFileEntry(fe) {
                                //                    $ionicLoading.hide();
                                //                    $scope.imgFile = fe.toURL();

                                //                    var im = $scope.imgFile;
                                //                    alert(im+"imagewhat?")
                                //                },
                                //                function (error) {
                                //                    $ionicLoading.hide();
                                //                    alert("Error getting file");
                                //                }
                                //            );
                                //        }
                                //    );
                                //},
                                //    function () {
                                //        $ionicLoading.hide();
                                //        alert("Error requesting filesystem");
                                //    });
                                targetPath = cordova.file.documentsDirectory + filename;
                                //targetPath = "/var/mobile/Applications/NoCloud/" + filename;
                            }

                            var trustHosts = true;
                            var options = {};
                            $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                                .then(function (result) {
                                    document.getElementById("videodiv").style.display = "block";
                                    document.getElementById("custom-overlay").style.display = "none";
                                    localStorage.setItem('videoSrcLocalURL', targetPath);
                                    $scope.videoSrcApply = targetPath;
                                    var test = $scope.videoSrcApply;
                                    var myVideo = document.getElementsByTagName('video')[0];
                                    myVideo.src = test;
                                    myVideo.load();
                                    myVideo.play();
                                    videoUrl = test;
                                    setTimeout(function () {
                                        document.getElementById("videodiv").style.display = "none";
                                        $scope.HideVideo = true;
                                        $scope.IsProcess = true;
                                        $scope.$apply();
                                    }, 20000);
                                }, function (error) {
                                    document.getElementById("custom-overlay").style.display = "none";
                                    alert("An error has occurred: Code = " + error.code);
                                    alert("upload error source " + error.source);
                                    alert("upload error target " + error.target);
                                }, function (progress) {

                                });
                            //}
                        })
                        .error(function (data) {
                        });
                }, 1000);
            }
            else {
                document.getElementById("custom-overlay").style.display = "none";
                document.getElementById("videodiv").style.display = "none";
            }

            $http.get("http://cs.innfocus.pt/api/secondScreen")
                .success(function (data) {
                    $scope.MainscreenText = data;
                    setTimeout(function () {
                        $ionicSlideBoxDelegate.update();
                    }, 1000);
                    var element = document.getElementsByTagName("ion-view");
                    $scope.clientHeight = element[0].clientHeight + "px";
                    var elementimg = document.getElementsByClassName("mainscreenimage");
                    $scope.clientImgHeight = element[0].clientHeight - 200 + "px";
                    //if (!$scope.IsProcess) {
                    //    setTimeout(function () {
                    //        $scope.HideVideo = true;
                    //        $scope.IsProcess = true;
                    //        $scope.$apply();
                    //    }, 22000);
                    //}
                })
                .error(function (data) {
                });

        };
        $scope.next = function () {
            $ionicSlideBoxDelegate.next();
        };
        $scope.previous = function () {
            $ionicSlideBoxDelegate.previous();
        };

        // Called each time the slide changes
        $scope.slideChanged = function (index) {
            $scope.slideIndex = index;
        };
        //$scope.sliderOptions = {
        //    effect: 'slide',
        //    pagination: false,
        //    initialSlide: 0
        //}
        $scope.myActiveSlide = 1;

        $scope.openProductList = function () {
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
                historyRoot: true
            });
            $state.go('app.search', { cache: false, reload: true });
        };
        $scope.options = {

        };

        $scope.data = {};
        $scope.downloadProgress = 0;
        $ionicHistory.nextViewOptions({
            historyRoot: true
        });

        $scope.loadAbout = function () {
            $http.get("http://cs.innfocus.pt/api/getAbout")
                .success(function (data) {
                    $scope.aboutText = data[0].about;
                })
                .error(function (data) {
                });
        }
        $scope.loadProduct = function () {


            $http.get("http://cs.innfocus.pt/api/thirdScreen")
                .success(function (data) {
                    $scope.productsList = data;
                })
                .error(function (data) {
                });

        }

        var inAppBrowserRef;
        var options = {
            location: 'no',
            //clearcache: 'yes',
            toolbar: 'yes',
            hardwareback: 'yes',
            closebuttoncaption: 'Done'
        };
        $ionicModal.fromTemplateUrl('templates/epubModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.epubModal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeepubModal = function () {
            $scope.epubModal.hide();
        };

        // Open the login modal
        $scope.OpenepubModal = function (url) {
            $scope.epubModal.show();
            $scope.loadEpub();
        };

        $scope.ePubURL = "";
        $scope.Book = "";
        $scope.loadEpub = function () {
            EPUBJS.Render.Iframe.prototype.setLeft = function (leftPos) {
                this.document.documentElement.style.WebkitTransform = "translate(-" + leftPos + "px, 0)";
            };
            EPUBJS.cssPath = "reader/";
            $scope.Book = "";
            document.getElementById("area").innerHTML = "";
            $scope.Book = ePub($scope.ePubURL);
            var divid1 = document.getElementById("area");
            $scope.Book.renderTo(divid1);
        };

        $scope.BookprevPage = function () {
            $scope.Book.prevPage();
        }

        $scope.BooknextPage = function () {
            $scope.Book.nextPage();
        }
        $scope.callBrowser = function () {
            Book = null;
            Book = ePub(BookPath);
            Book.renderTo("area");
        };

        $scope.openBrowser = function (epubFileUrl) {

            var ref = cordova.InAppBrowser.open(epubFileUrl, '_blank', 'location=yes');
            ref.addEventListener('loadstart', function (event) {
            });

            //var nabLoader = document.getElementById("myNavLoader");
            //nabLoader.style.display = "block";

            //if (epubFileUrl != "") {

            //    var url = epubFileUrl;
            //    var filename = url.split('/').pop().split('#')[0].split('?')[0];
            //    if (filename == "") {
            //        filename = "onepub.epub";
            //    }
            //    var targetPath = cordova.file.dataDirectory + filename;
            //    if ($ionicPlatform.is('android')) {
            //        var targetPath = cordova.file.externalApplicationStorageDirectory + filename;
            //    }

            //    if ($ionicPlatform.is('ios')) {
            //        var targetPath = cordova.file.dataDirectory + filename;
            //    }

            //    var trustHosts = true;
            //    var options = {};

            //    $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
            //        .then(function (result) {
            //            $scope.ePubURL = targetPath;
            //            BookPath = targetPath;
            //            nabLoader.style.display = "none";
            //            $scope.OpenepubModal(targetPath);
            //        }, function (error) {
            //            // Error
            //            nabLoader.style.display = "none";
            //            alert("An error has occurred: Code = " + error.code);
            //            alert("upload error source " + error.source);
            //            alert("upload error target " + error.target);
            //        }, function (progress) {
            //            $timeout(function () {
            //                //var barvalue = parseFloat((progress.loaded / progress.total) * 100).toFixed(2);
            //                document.getElementById("progressbar").setAttribute("value", (progress.loaded / progress.total) * 100);

            //                //document.querySelector('#progressbar').addEventListener('mdl-componentupgraded', function () {                               
            //                //    this.MaterialProgress.setProgress(barvalue);
            //                //});
            //                //document.getElementById("progressbar").value = parseFloat((progress.loaded / progress.total) * 100).toFixed(2);
            //            });
            //        });

            //    //var ref = cordova.InAppBrowser.open(epubFileUrl, '_blank', 'location=yes');
            //    //ref.addEventListener('loadstart', function (event) {
            //    //});
            //}
            //else {
            //    nabLoader.style.display = "none";
            //}
        }


    });
